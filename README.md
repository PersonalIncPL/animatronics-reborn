# Animatronics Reborn

## What is this project?
This project was meant to be a FNaF fangame, which has failed due to my lack of free time and expertise at the time?

## Will the project be ever continued?
Possibly in the future.

## Can I commit to this project?
No. If you want to continue development, sync the project onto your local machine. Make sure to credit me for my code, and GaboCoArt for the models.

## Can I sell the compiled binaries?
No. The project is open-source. You do not have any right to sell the compiled binaries of A:R under any condition.
