﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {
	public int pos = 1;
	public Animator CameraAnimator;
	public bool canMove = true;
	
	// Update is called once per frame
	void Update () {
		// Check if player can move
		if (canMove) {
			// From Front to Left
			if (Input.GetKeyDown(KeyCode.A) && pos == 1) {
				if (!CameraAnimator.GetCurrentAnimatorStateInfo(0).IsName("idle") && CameraAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1.0f) {
					pos = 0;
					CameraAnimator.Play("front to left");
				} else {
					pos = 0;
					CameraAnimator.Play("front to left");
				}
			}

			// From Front to Right
			if (Input.GetKeyDown(KeyCode.D) && pos == 1) {
				if (!CameraAnimator.GetCurrentAnimatorStateInfo(0).IsName("idle") && CameraAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1.0f) {
					pos = 2;
					CameraAnimator.Play("front to right");
				} else {
					pos = 2;
					CameraAnimator.Play("front to right");
				}
			}

			// From Left to Front
			if (Input.GetKeyDown(KeyCode.D) && pos == 0 && CameraAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1.0f) {
				pos = 1;
				CameraAnimator.Play("left to front");
			}

			// From Right to Front
			if (Input.GetKeyDown(KeyCode.A) && pos == 2 && CameraAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1.0f) {
				pos = 1;
				CameraAnimator.Play("right to front");
			}
		}
	}
}
