﻿using UnityEngine;
using UnityEngine.UI;

public class FreddyController : MonoBehaviour {
	public GameObject go;
	public CameraShake CameraShake;
	public RawImage bloodOnScreen;
	public CameraMovement CameraMovement;
	public Animator LightAnimator;
	void Update () {
		if (Input.GetKeyDown(KeyCode.K)) {
			CameraShake.ShakeCamera(10f, 10f);
			go.SetActive(true);
		}
		if (go.GetComponent<Animator>().isActiveAndEnabled && go.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime > 1f) {
			LightAnimator.Play("lightAnim");
			CameraMovement.canMove = false;
			bloodOnScreen.enabled = true;
			bloodOnScreen.color = Color.Lerp(Color.white, Color.black, go.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime - 1f);
		}
		
	}
}
