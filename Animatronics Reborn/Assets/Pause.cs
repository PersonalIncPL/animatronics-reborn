﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.UI;

public class Pause : MonoBehaviour {

	public bool isPaused = false;
	public bool canPause = true;
	public GameObject PausedCanvas;
	public PostProcessingProfile ColorCorrection;
	public Toggle bloom;
	public Toggle motionBlur;
	public Toggle ambientOcclusion;
	public Toggle vignette;
	public Toggle chromaticAberration;
	string SettingsPath;
	void Start() {
		SettingsPath = Application.persistentDataPath.Replace("/", "\\");

		// Add listeners to pause toggles
		bloom.onValueChanged.AddListener(ToggleBloom);
		motionBlur.onValueChanged.AddListener(ToggleMB);
		ambientOcclusion.onValueChanged.AddListener(ToggleAO);
		vignette.onValueChanged.AddListener(ToggleVignette);
		chromaticAberration.onValueChanged.AddListener(ToggleCA);

		// Read and set Bloom Settings. If can't read, fallback to on.
		try {
			string result = File.ReadAllText(SettingsPath + "\\settings-bloom.txt");
			bool boolean = result == "on";
			bloom.isOn = boolean;
			ToggleBloom(boolean);
		} catch {
			Debug.LogWarning("Couldn't read Bloom settings. Fallback to on!");
			File.WriteAllText(SettingsPath + "\\settings-bloom.txt", "on");
			bloom.isOn = true;
			ToggleBloom(true);
		}

		// Read and set Motion Blur Settings. If can't read, fallback to on.
		try {
			string result = File.ReadAllText(SettingsPath + "\\settings-mb.txt");
			bool boolean = result == "on";
			motionBlur.isOn = boolean;
			ToggleMB(boolean);
		} catch {
			Debug.LogWarning("Couldn't read Motion Blur settings. Fallback to on!");
			File.WriteAllText(SettingsPath + "\\settings-mb.txt", "on");
			motionBlur.isOn = true;
			ToggleMB(true);
		}

		// Read and set Ambient Occlusion Settings. If can't read, fallback to on.
		try {
			string result = File.ReadAllText(SettingsPath + "\\settings-ao.txt");
			bool boolean = result == "on";
			ambientOcclusion.isOn = boolean;
			ToggleAO(boolean);
		} catch {
			Debug.LogWarning("Couldn't read Ambient Occlusion settings. Fallback to on!");
			File.WriteAllText(SettingsPath + "\\settings-ao.txt", "on");
			ambientOcclusion.isOn = true;
			ToggleAO(true);
		}

		// Read and set Vignette Settings. If can't read, fallback to on.
		try {
			string result = File.ReadAllText(SettingsPath + "\\settings-vignette.txt");
			bool boolean = result == "on";
			vignette.isOn = boolean;
			ToggleVignette(boolean);
		} catch {
			Debug.LogWarning("Couldn't read Vignette settings. Fallback to on!");
			File.WriteAllText(SettingsPath + "\\settings-vignette.txt", "on");
			vignette.isOn = true;
			ToggleVignette(true);
		}

		// Read and set Chromatic Aberration Settings. If can't read, fallback to on.
		try {
			string result = File.ReadAllText(SettingsPath + "\\settings-ca.txt");
			bool boolean = result == "on";
			chromaticAberration.isOn = boolean;
			ToggleCA(boolean);
		} catch {
			Debug.LogWarning("Couldn't read Chromatic Abberation settings. Fallback to on!");
			File.WriteAllText(SettingsPath + "\\settings-ca.txt", "on");
			chromaticAberration.isOn = true;
			ToggleCA(true);
		}
	}

	void ToggleBloom(bool toggle) {
		ColorCorrection.bloom.enabled = toggle;
		if (toggle) File.WriteAllText(SettingsPath + "\\settings-bloom.txt", "on"); else File.WriteAllText(SettingsPath + "\\settings-bloom.txt", "off");
	}
	void ToggleMB(bool toggle) {
		ColorCorrection.motionBlur.enabled = toggle;
		if (toggle) File.WriteAllText(SettingsPath + "\\settings-mb.txt", "on"); else File.WriteAllText(SettingsPath + "\\settings-mb.txt", "off");
	}
	void ToggleAO(bool toggle) {
		ColorCorrection.ambientOcclusion.enabled = toggle;
		if (toggle) File.WriteAllText(SettingsPath + "\\settings-ao.txt", "on"); else File.WriteAllText(SettingsPath + "\\settings-ao.txt", "off");
	}
	void ToggleVignette(bool toggle) {
		ColorCorrection.vignette.enabled = toggle;
		if (toggle) File.WriteAllText(SettingsPath + "\\settings-vignette.txt", "on"); else File.WriteAllText(SettingsPath + "\\settings-vignette.txt", "off");
	}

	void ToggleCA(bool toggle) {
		ColorCorrection.chromaticAberration.enabled = toggle;
		if (toggle) File.WriteAllText(SettingsPath + "\\settings-ca.txt", "on"); else File.WriteAllText(SettingsPath + "\\settings-ca.txt", "off");
	}
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape) && canPause) {
			if (!isPaused) {
				PausedCanvas.SetActive(true);
				Time.timeScale = 0f;
				isPaused = true;
			} else if (isPaused) {
				PausedCanvas.SetActive(false);
				Time.timeScale = 1f;
				isPaused = false;
			}
		}
	}
}
